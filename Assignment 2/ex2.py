"""
ex2.py
A tool used to read data from a file and display the information in a table based on the users request, in this case the data is about
cities and there distances.
Version: 0.9
Author: Callum-James Smith (cs18804)
Date: 7/12/18
"""

class bColours:
    """
    Colours used to provide pretty-ness
    !This only works on *nix systems!
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

def menu():
    """
    The menu that will allow users to select which task they wish to use.

    Args:
        No arguments
    Vars:
        sel: The users selection from the menu
    """
    print('_'*45)
    print("""
    File:""" + bColours.OKGREEN + filename + ' loaded \u2713' + bColours.ENDC + """
    1. List all city data
    2. Search -->
    9. Calculate distance of cities
    0. Exit
    """)
    print('_'*45)
    sel = int(input("Please make a selection> "))
    if sel == 1:
        load_data(filename, sel)
    elif sel == 2:
        """
        This is the search menu

        Vars:
            sSel: The selection made by the user for the search menu
        """
        # Display the search menu
        print('_'*45)
        print(bColours.OKBLUE +'Welcome!\n' + bColours.ENDC + """
        1. Search by city name
        2. Search by population
        3. Search by coordinates
        9. Back
        0. Quit
        """)
        print('_'*45)
        sSel = int(input("Please choose an option> "))
        if sSel == 1: # If the user selects option 1 from the search menu
            sel = 2 # Set the main menu selection == 2
            load_data(filename, sel) # Call the load_data() function passing over the filename and selection
        elif sSel == 2:
            sel = 3
            load_data(filename, sel)
        elif sSel == 3:
            sel = 4
            load_data(filename, sel)

        elif sSel == 9:
            menu()
        elif sSel == 0:
            print("Goodbye!")
            return None

    elif sel == 9:
        load_data(filename, sel)

def load_data(filename, sel):
    """
    This is where the file is opened and sorted into a list of tuples

    Args:
        filename: This is the name of the file obtained from the user input from the main function
        sel: This is the selection the user makes from the main menu and the search menu

    Vars:
        cities: This stores each line of the file
        city: Stores each 'word' from the line
        c1: An empty list that will store any integer values
        test: This stores each word from the line city, with any non-alpha chars removed

    """
    try:
        with open(filename, 'r', newline='') as pf:
            cities = pf.read().splitlines()
            for x in cities:
                city = x.split()
                c1 = []
                for j in city:
                    test = j.replace(",", 'r').replace("-", '')
                    if test.isalpha():
                        c1.append(j)
                    else:
                        c1.append(int(test))
                city_data.append(tuple(c1))
        output_data(sel)
    except FileNotFoundError:
        print(bColours.FAIL + "Error!: File does not exist!" + bColours.ENDC)

def output_data(sel):
    """
    This is used to output the data into a nice table

    Args:
        sel: This is the selection made by the user
    
    Vars:
        name: This stores the value of the name of a city which the user specifies from the input
        minVal: This stores the minimum value for the population of a city for use in the 'search by population' option
        maxVal: The maximum value for the 'search by population' option
        Latitude: This is the value inputted by the user to search for a city within a certain range
        Longitude: This is the value inputted by the user to search for a city within a certain range
        userCoord: This is a value that stores the sum of the Latitude and Longitude value
    """
    if sel == 1:
        col_print("full")
        for x in city_data:
            tuple_print(x, "full")
        print("-"*58)
    
    elif sel == 2:
        try:
            name = input("Please enter the name of the city> ")
            col_print("full")
            for x in city_data:
                if name in x[0]:
                    tuple_print(x, "full")
            print('_'*58)
        except ValueError:
            print(bColours.WARNING + "Please use a string value!" + bColours.ENDC)
            output_data(sel)
    
    elif sel == 3:
        try:
            minVal = int(input("Please enter the minimum population> "))
            maxVal = int(input("Please enter the maximum population> "))
            col_print("full")
            for x in city_data:
                if minVal <= x[3] <= maxVal:
                    tuple_print(x, "full")
            print("_"*58)
        except ValueError:
            print(bColours.WARNING + "Please enter an integer!" + bColours.ENDC)
    
    elif sel == 4:
        """
        This searches by coordinates, the user enters a value and the program will find a city
        within 10Km of that value.

        Args:

        """
        Latitude = int(input("Please enter the latitude(in Km)> "))
        Longitude = int(input("Please enter the longitude(in Km)> "))
        userCoord = Latitude+Longitude
        print(userCoord)
        col_print("full")
        for x in city_data:
            coord = x[1]+x[2]
            if userCoord <= coord <= userCoord+10:
                tuple_print(x, "full")
        print("_"*58)

    elif sel == 9:
        """
        Calculate the distance between two cities.
        Unable to complete due to time constraints.

        My thought process to have te user specify two cities, it would then find the longitude and latitude of the two cities.
        It would then compare to see which value was bigger and take the biggest value away from the smallest one, this *should*
        give the result being the distance between the two specified cities.
        """
        city1 = input("Please enter the name of the first city> ")
        city2 = input("Please enter the name of the second city> ")
        for x in city_data:
            city1Val = x[1]+x[2]
            city2Val = x[1]+x[2]
            if city1Val < city2Val:
                distance = city2Val-city1Val
            elif city1Val > city2Val:
                distance = city1Val-city2Val
        print("The distance between " + city1 + ' and ' + city2 + ' is ' + distance)

def tuple_print(x, type):
    """
    This is a function that uses formats to print the data into the table

    Args:
        x: This is the individual line for the city_data list
        type: This is a method in which the program can choose between two different printing modes, one displaying all of the data and the
        other one only displaying the name of the city and the number of citizens.

    Vars:
        table: This is the string that will print the row of data
    """
    if type == "full":
        table = "{:<30} {:<10} {:<10} {:<8}"
        print(table.format(x[0], x[1], x[2], x[3]))
    elif type == "restricted":
        table = "{:<30} n. Citizens: {:<8}"
        print(table.format(x[0], x[3]))
    
def col_print(colType):
    """
    This function is used to print the column above the table

    Args:
        colType: This argument is used to specify which columns are shown
    
    Vars:
        col: This is the string that will be used to print out the column according to the colType specified by the program
    """
    if colType == "full":
        col = "{:^30} {:^10} {:^10} {:^8}"
        print("-"*58)
        print(col.format(column[0], column[1], column[2], column[3]))
        print("-"*58)
    elif colType == "restricted":
        col = "{:^30} {:^8}"
        print("-"*38)
        print(col.format(column[0], column[3]))
        print("-"*38)

if __name__ == '__main__':
    """
    The main body of the program

    Vars:
        city_data: An empty global list that holds the city data tuples
        column: This is a tuple that stores the columns that are displayed above the table
        filename: A user input that stores the name of the file they wish to use
    """
    city_data = []
    column = ('City Name', 'Latitude', 'Longitude', 'Population')
    filename = input("Please enter the filename> ")
    try:
        open(filename, 'r')
        menu()
    except FileNotFoundError:
        print(bColours.FAIL + "Error!: File does not exist!" + bColours.ENDC)