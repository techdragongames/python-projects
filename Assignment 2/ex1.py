"""
ex1.py
Created by: Callum-James Smith
Date: 29/11/18
Version: 0.8
A tool used to read data from a file, store it and display details according to the users choices.
"""

class bColours:
    """
    Colours used to provide pretty-ness
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def menu():
    """
    The menu

    Args:
        filename: refers to the filename input
        sel: this is the selection the user makes
    """
    print('-'*45)
    print("""
    Welcome!\n
    File:""" + bColours.OKGREEN + filename + ' loaded \u2713' + bColours.ENDC + """
    1. List all player data
    2. Search -->
    0. Exit
    """)
    print('-'*45)
    sel = int(input("Please choose an option> "))
    if sel == 1: # If the users selection is equal to 1
        load_data(filename, sel) # Run the load_data() function
    elif sel == 2: # If the users selection is equal to 2
        try:
            # Display the search menu
            print('-'*45)
            print(bColours.OKBLUE +'Welcome!\n' + bColours.ENDC + """
            1. Search by surname
            2. Search by salary
            3. Search by team
            9. Back
            0. Exit
            """)
            print('-'*45)
            sSel = int(input("Please choose an option> ")) # stores the users selection
            if sSel == 1: # If the selection is 1
                sel = 2 # Set the menu() selection equal to 2 | this is to allow the conditions in the output_data() to work
                load_data(filename, sel)
            elif sSel == 2:
                sel = 3
                load_data(filename, sel)

            elif sSel == 3:
                sel = 4
                load_data(filename, sel)

            elif sSel == 9: # If the user selects 9, they will be brought to the main menu
                menu()
            elif sSel == 0: # If the user selects 0, the program will quit
                print("Goodbye!")
                return None
        except ValueError:
            print(bColours.WARNING + "Warning!: Please enter a number!" + bColours.ENDC)
            menu()

    elif sel == 0: # If the user selects 0, the program will quit
        print("Goodbye!")
        return None

def load_data(filename, sel):
    """
    Open the file using a with statement, then reads and splits via the lines, each charcater is then split.
    Finally, the program will create an empty list, and the program will test if there are any alpha chars; if so it will append it to the list.

    Args:
        filename: Refers to the filename given by the user
        sel: The selection carried over from menu() --> load_data()
        pf: The alternative name given for filename
        players: This is a var that stores each line
        pSplit: Stores the individual characters
        l1: This is the empty list that stores the salary values in a base-10 int format, this is then appended to test
        player_data: This references the empty list from the main body of the program
    """
    try:
        with open(filename, 'r', newline='') as pf:
            players = pf.read().splitlines()
            for i in players:
                pSplit = i.split()
                l1 = []
                for j in pSplit:
                    test = j.replace(",", '').replace("-", '')
                    if test.isalpha():
                        l1.append(j)
                    else:
                        l1.append(int(test))
                player_data.append(tuple(l1))
        output_data(sel)
    except FileNotFoundError:
        print(bColours.FAIL + "Error!: File does not exist!" + bColours.ENDC)

def output_data(sel):
    """
    This is the real juicy part of the program, it provides the output to the console

    Args:
        sel: This is the selection carried over from menu() --> load_data() --> output_data()
        surname: This is the user requested name provided by the user
        minVal: This is the minimum value the user wishes to search for
        maxVal: This is the maximum value the user wishes to search for
        teamName: A variable that stores the name of the team the user wishes to search for
    """
    if sel == 1:
        col_print('full')
        for x in player_data:
            tuple_print(x, "full")
        print("-"*95)
        verification()

    elif sel == 2:
        try:
            surname = input("Please enter the name you wish to find> ")
            col_print("full")
            for x in player_data:
                if surname in x[3]:
                    tuple_print(x, "full")
            print("-"*95)
            verification()
        except ValueError:
            print(bColours.WARNING + "Warning!: Please enter a string!" + bColours.ENDC)
            output_data(sel)

    elif sel == 3:
        try:
            minVal = int(input("Please enter the minimum value> "))
            maxVal = int(input("Please enter the maximum value> "))
            col_print("full")
            for x in player_data:
                if minVal <= x[4] <= maxVal:
                    tuple_print(x, "full")
            print("-"*95)
            verification()
        except ValueError:
            print(bColours.WARNING + "Warning!: Please enter a number!" + bColours.ENDC)
            output_data(sel)

    elif sel == 4:
        try:
            teamName = input("Please enter the name of the team> ")
            col_print("restricted")
            for x in player_data:
                if teamName in x:
                    tuple_print(x, "restricted")
            print("-"*60)
            verification()
        except ValueError:
            print(bColours.WARNING + "Warning!: Please enter a number!" + bColours.ENDC)
            output_data(sel)

def tuple_print(x, type):
    """
    The printing of the tuples using formatting, this function is called by output_data()

    Args:
        x: Refers to each item in a tuple
        type: This is a method to switch between the different amounts of information being printed
        table: stores the string that will be printed upon request, there is also a special {:<10,d} operator that allows the salary to be output with CSVs
    """
    if type == "full":
        table = "{:<30} {:<30} {:<10,d} {:<15} {:<7}"
        print(table.format(x[2], x[3], x[4], x[1], x[0]))
    elif type == "restricted":
        table = "{:<30} {:<30}"
        print(table.format(x[2], x[3]))

def col_print(colType):
    """
    This function prints the top header(column)

    Args:
        colType: This is another method that allows switching to different header 'modes'
        col: A variable that stores the string that'll be printed
    """
    if colType == "full":
        col = "{:<30} {:<30} {:<10} {:<15} {:<7}"
        print("-"*95)
        print(col.format(column[0], column[1], column[2], column[3], column[4],))
        print("-"*95)
    elif colType == "restricted":
        col = "{:<30} {:<30}"
        print("-"*60)
        print(col.format(column[0], column[1]))
        print("-"*60)

def verification():
    """
    A function used to ask the user of they wish to proceed etc.

    Args:
        ans: Stores the users selection
    """
    ans = input("Would you like to (e)Exit or (m)Return to menu?> ")
    if ans == 'e':
        return None
    elif ans == 'm':
        menu()

if __name__ == '__main__':
    """
    The main body of the program

    Args:
        player_data: This is an empty list and allows the list to be used globally
        column: This stores the tuple for the column that is used for the printing of the tables
        filename: Will ask the user to input a string attaining to the name of the file they wish to open
    """
    player_data = []
    column = ('first name', 'Surname', 'salary', 'position', 'Team')
    filename = input("Please enter the filename> ")
    try:    
        open(filename, 'r')
        menu()
    except FileNotFoundError:
        print(bColours.FAIL + "Error!: File does not exist!" + bColours.ENDC)